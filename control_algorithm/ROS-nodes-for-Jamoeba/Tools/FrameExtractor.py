'''
Python script to extract
the frames of a video
'''

####################################################
import cv2
####################################################


video_name = 'Three_Jammed_Moving.mp4'
video_capture = cv2.VideoCapture(video_name)
success, image = video_capture.read()
count = 0

while success:
	cv2.imwrite("frames/frame%d.jpg" % count, image)     # save frame as JPEG file
	success, image = video_capture.read()
	print('Read a new frame: ', success)
	count += 1
