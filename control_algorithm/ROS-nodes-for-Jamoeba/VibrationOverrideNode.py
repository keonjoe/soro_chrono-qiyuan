'''
Python script used to override the control
loop of the vibration robot
'''

###################################################
from Source.Application import OverrideApplication
###################################################


# Initialise the app
app = OverrideApplication()
