import numpy as np
import argparse
import os
import sys
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from environment import Environment

def main():
    file_no         = int(sys.argv[1])
    n_tau           = 5
    tau_vs          = np.logspace(1, 3, n_tau)
    tau_thetas      = np.logspace(1, 3, n_tau)
    filename        = '%03d' % file_no
    env = Environment(filename, tau_v=tau_vs[file_no%n_tau], tau_theta=tau_thetas[file_no//n_tau])
    env.simulation()
    env.make_video()
    env.check_success()
    Environment.gather_success()

if __name__ == '__main__':
    main()