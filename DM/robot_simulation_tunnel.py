# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 16:07:52 2019

@author: dmulr
"""



# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
# In[Set Path]
chrono.SetChronoDataPath("data/")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
#my_system.SetSolverType(chrono.ChSolver.Type_BARZILAIBORWEIN)
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.001)
my_system.SetMaxItersSolverSpeed(1000)
# In[surface materialcreate material]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(.3)
material.SetDampingF(.0)
material.SetCompliance (0.00001)
material.SetComplianceT(0.00001)
material.SetRollingFriction(.4)
material.SetSpinningFriction(.4)
material.SetComplianceRolling(0.00001)
material.SetComplianceSpinning(0.00001)

bot_material = chrono.ChMaterialSurfaceNSC()
bot_material.SetFriction(.3)
bot_material.SetDampingF(.0)
bot_material.SetCompliance (0.00001)
bot_material.SetComplianceT(0.00001)
bot_material.SetRollingFriction(.4)
bot_material.SetSpinningFriction(.4)
bot_material.SetComplianceRolling(0.00001)
bot_material.SetComplianceSpinning(0.00001)

# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(10, 1, 10) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(10, 1, 10)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall]

# In[Create wall]

wwidth=.1
wheight=.1
wlength=1.5

meh=np.array([1,-1])
#for i in(meh):
#    wall = chrono.ChBody()
#    wall.SetBodyFixed(True)
#    wall.SetPos(chrono.ChVectorD(1.5*i, wheight/2, 0))
#    wall.SetMaterialSurface(material)
#    # Collision shape
#    wall.GetCollisionModel().ClearModel()
#    wall.GetCollisionModel().AddBox(wwidth, wheight, wlength) # hemi sizes
#    wall.GetCollisionModel().BuildModel()
#    wall.SetCollide(True)
#    # Visualization shape
#    wall_shape = chrono.ChBoxShape()
#    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wwidth, wheight, wlength)
#    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
#    wall.GetAssets().push_back(wall_shape)
#    wall_texture = chrono.ChTexture()
#    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
#    wall.GetAssets().push_back(wall_texture)
#    my_system.Add(wall)

for i in(meh):
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(0, wheight/2, 1.5*i))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wlength, wheight, wwidth) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wlength,wheight, wwidth)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)


# In[Additional walls]
wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.7, wheight/2, -.85))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)

# In[Tunnel wall]
wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.2, wheight/2, -.75))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)



wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.7, wheight/2, .75))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)

# In[Tunnel wall]
wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.2, wheight/2, .85))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)


# In[cylinder dimmensions]
# run 1 97, k=1000, n=np.array([93,86,77,69,61,52,44,33,22,12,4])

# run 2 94, k=1000, n=np.array([93,86,77,69,61,52,44,33,22,12,4])

# run 3 94, k=1000, n=np.array([93,86,77,69,61,52,44,33,22,12,4])

# number of robots
nb=87
n=np.array([93,86,77,69,61,52,44,33,22,12,4])
ni=np.sum(n)

nt=nb+ni
# diameter of cylinder and robots
diameter=.07 

R1=(diameter*nb/(np.pi*2))+.15
# mass
mass=.18
# height of cylinder
height=.12
# calculate volume
volume=np.pi*.25*height*(diameter)**2
# calculate density
density=mass/volume

Inertia=.5*mass*(diameter/2)**2
# spring constant
k=1000
# damping
b=0

# empty matrix
obj=[]
# constant function for external forces



# ..create the function for imposed x horizontal motion, etc.
mfunX = chrono.ChFunction_Sine(0,.002,5)  # phase, frequency, amplitude
#link_shaker.SetMotion_Y(mfunY)
constfun = chrono.ChFunction_Const(3)

botcall=np.zeros((1,nb))


active=np.array([-6,-5,-4-3,-2,-1,0,1,2,3,4,5,6])

for i in (active):
    botcall[:,i]=1

# In[Create Bots]
for i in range (nb):
    x=R1*np.cos(i*2*np.pi/nb)
    y=.5*height
    z=R1*np.sin(i*2*np.pi/nb)
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyCylinder(diameter/2, height,density)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetMaterialSurface(bot_material)
    bot.SetMass(mass)
    bot.SetId(i)
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetF_x(constfun)
        myforcex.SetDir(chrono.ChVectorD(1,0,0))
    else:
        print('passive')
    col_g = chrono.ChColorAsset()
    col_g.SetColor(chrono.ChColor(0, 1, 0))
    bot.AddAsset(col_g)
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(obj[i-1], bot, False, chrono.ChVectorD(obj[i-1].GetPos().x,obj[i-1].GetPos().y ,obj[i-1].GetPos().z), chrono.ChVectorD(x, y, z))
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(diameter)
        my_system.AddLink(ground)
    if i==nb-1:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(bot, obj[0], False,chrono.ChVectorD(x, y, z) , chrono.ChVectorD(obj[0].GetPos().x,obj[0].GetPos().y ,obj[0].GetPos().z))
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(diameter)
        my_system.AddLink(ground)        
    my_system.Add(bot)
    obj.append(bot)        


# In[Create interiors]
for i in range(n.size):
    print(i)
    for j in range(n[i]):
        
        R2=diameter*n[i]/(np.pi*2)
        x=R2*np.cos(j*2*np.pi/n[i])
        y=.5*height
        z=R2*np.sin(j*2*np.pi/n[i])
        gran = chrono.ChBody()
        gran = chrono.ChBodyEasyCylinder(diameter/2, height,density)
        gran.SetPos(chrono.ChVectorD(x,y,z))
        gran.SetMaterialSurface(bot_material)
        gran.SetMass(mass)
        gran.SetId(i)
        gran.GetCollisionModel().ClearModel()
        gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
        gran.GetCollisionModel().BuildModel()
        gran.SetCollide(True)
        col_r = chrono.ChColorAsset()
        col_r.SetColor(chrono.ChColor(1, 0, 0))
        gran.AddAsset(col_r)
        my_system.Add(gran)
        obj.append(gran) 


        


Xpos=[]
Ypos=[]
Zpos=[]
Xforce=[]
Yforce=[]
Zforce=[]
Xcontact=[]
Ycontact=[]
Zcontact=[]
time=[]
count=0

# In[Set up pov exporter]
pov_exporter = postprocess.ChPovRay(my_system)

# Sets some file names for in-out processes.
pov_exporter.SetTemplateFile("data/_template_POV.pov")
pov_exporter.SetOutputScriptFile("rendering.pov")
pov_exporter.SetOutputDataFilebase("my_state")
pov_exporter.SetPictureFilebase("picture")

# create folders
if not os.path.exists("output"):
    os.mkdir("output")
if not os.path.exists("anim4"):
    os.mkdir("anim")
pov_exporter.SetOutputDataFilebase("output/my_state")
pov_exporter.SetPictureFilebase("anim/picture")

# Export out objects
pov_exporter.AddAll() 
pov_exporter.SetCamera(chrono.ChVectorD(1.3,3,0), chrono.ChVectorD(1.3,0,0), 90)# specifiy camera location
pov_exporter.ExportScript()
 #In[Run the simulation]
count=0
while (my_system.GetChTime() < 30) :
    count=count+1
    my_system.DoStepDynamics(0.01)
    print ('time=', my_system.GetChTime())
    
    if count%3==0:
        pov_exporter.ExportData()
    
          
#    for i in range(nt):
#        temp=obj[i].GetContactForce()
#        tempx=obj[i].Get_Xforce()
#        Xforce.append(tempx.x)
#        Yforce.append(tempx.y)
#        Zforce.append(tempx.z)
#        Xcontact.append(temp.x)
#        Ycontact.append(temp.y)
#        Zcontact.append(temp.z)
#        Xpos.append(obj[i].GetPos().x)
#        Ypos.append(obj[i].GetPos().y)
#        Zpos.append(obj[i].GetPos().z)
#    count=count+1
#
#
#
## positiond
#qx=np.zeros((nt,count))
#qy=np.zeros((nt,count))
#qz=np.zeros((nt,count))
## contact forces
#Fxc=np.zeros((nt,count))
#Fyc=np.zeros((nt,count))
#Fzc=np.zeros((nt,count))
## total forces
#Fxt=np.zeros((nt,count))
#Fyt=np.zeros((nt,count))
#Fzt=np.zeros((nt,count))
#
#
#constant=.06*np.ones((nb+ni,count))
#for i in range(count):
#    qx[:,i]=Xpos[nt*i:nt*i+nt]
#    qy[:,i]=Ypos[nt*i:nt*i+nt]  
#    qz[:,i]=Zpos[nt*i:nt*i+nt]  
#    
#    Fxt[:,i]=Xforce[nt*i:nt*i+nt] 
#    Fyt[:,i]=Yforce[nt*i:nt*i+nt] 
#    Fzt[:,i]=Zforce[nt*i:nt*i+nt] 
#    
#    Fxc[:,i]=Xcontact[nt*i:nt*i+nt] 
#    Fyc[:,i]=Ycontact[nt*i:nt*i+nt] 
#    Fzc[:,i]=Zcontact[nt*i:nt*i+nt] 
#qy=qy-constant
#np.savez('data_chrono.npz',allow_pickle=True,qx=qx,qy=qy,qz=qz,Fxt=Fxt,Fyt=Fyt,Fzt=Fzt,Fxc=Fxc,Fyc=Fyc,Fzc=Fzc,nb=nb,ni=ni,nt=nt,time=time)

#obj = np.asarray(obj, dtype=np.float32)
#np.savez('resume1.npz',obj=obj,ni=ni,nb=nb,mass=mass,height=height,diameter=diameter, allow_pickle=True, fix_imports=True)
