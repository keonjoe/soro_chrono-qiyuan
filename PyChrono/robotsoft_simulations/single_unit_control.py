# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 15:19:22 2019

@author: dmulr
"""

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
from pid_controller.pid import PID
import math as math




####################################################
class StringController:

	def __init__(self, kp=1., ki=0., kd=0., disabled=0):
		""" Initialise the instance of the class """
		# States and angles
		self.state = {"0": (0, 0, 0)}
		self.pot_angle = (0)
		self.current_angle = array([0])
		self.pwm = array([0, 0])
		self.last = clock()
		self.angle_threshold = 8
		self.disabled = disabled

		# Initialize the controllers
		self.pid = array([None])
		for i in range(8):
			self.pid[i] = PID(p=kp, i=ki, d=kd)

		# Set target angles
		self.target_angles = (0)
		self.set_target_angle()



	def state_callback(self, data):
		""" Method to parse data from the state topic """
		self.state = eval(data.data)

	def angle_callback(self, data):
		""" Method to parse data from the angle topic """
		self.pot_angle = eval(data.data)

	def set_target_angle(self, tar=(0.)):
		""" Method to set the target """
		self.target_angles = tar
		for i in range(8):
			self.pid[i].target = tar[i]


#start control loop 2
	def start_control_loop2(self):
		""" Method to start the control loop """
		error_a = np.ones(1)  # Initialise the error array
		command = np.ones(1)  # Initialise the command array
		self.angle_threshold = 20  # Change the threshold value to 20 degrees
		
		# Start the control loop
		while True:
			angles_are_set = sum(1 * (error_a < self.angle_threshold)) == 1  # Verify if every angles are within the tolerances

			for i in range(self.disabled, 8):
				self.current_angle[i] = (self.state[str(i)][2] + self.pot_angle[i]) % 360  # Bring the angle between 0 and 360
				if self.current_angle[i] > 180:
					self.current_angle[i] += -360  # Bring the angle back between -180 and 180

				command[i] = -self.pid[i](feedback=self.current_angle[i])  # Compute the command from the current state
				error_a[i] = self.pid[i].error  # Get the error

				if angles_are_set:  # If the angles of every robots are within 20 degrees of the target
					# Adjust the angles slightly without stop
					self.pwm[2*i] = 80 + command[i]
					self.pwm[2*i + 1] = 80 - command[i]
				else:  # Else, stop and get the angles of every robots within 20 degrees of the target
					if abs(error_a[i]) > self.angle_threshold:  # If the error is bigger than 20 degrees, adjust it
						self.pwm[2*i] = max(min(command[i], 255), -255)  # Clamp the command between -255 and 255
						self.pwm[2*i + 1] = -self.pwm[2*i]
					else:
						self.pwm[2*i] = self.pwm[2*i + 1] = 0  # If the angle is within 20 degrees of the target, do nothing

#####################################################################################################################################
#####################################################################################################################################
#####################################################################################################################################
#####################################################################################################################################

# Forces is a row matrix
# state is a row matrix [x,y,theta]
# desired is a constant so desired heading
# error is allowable error
                        
def Controller(state,desired,error,Forces,ki,kp,kd):                   
    
    fpwm=np.zeros((1,2))# empty matrix convert forces to pwm
    pwmf=np.zeros((1,2))# empty matrix to convert pwm to forces
    command=np.zeros((1,2)) # empty matrix for output forces
    fpwm[0]=Force_pwm(Forces[0])
    fpwm[1]=Force_pwm(Forces[1])
    
    if abs(desired-state[2])<error:
        pwmf[0]=pwm_Force(fpwm[0])
        pwmf[1]=pwm_Force(fpwm[1])
        
    else:
       command= 
# In[Convert Force to pwm]                       
def pwm_Force(x):
    a=.0015
    b=1.0352
    pwm=a*x+b
    return pwm

# In[Convert pwm to Force]
def Force_pwm(x):
    a=666
    b=690
    Force=a*x+b
    return Force
# In[Run program]
sim=0
start = timeit.default_timer()
record=0
run=10



# In[Set Path]
chrono.SetChronoDataPath("../../../data/")


# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.00001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.00001)
my_system.SetMaxItersSolverSpeed(1000)
my_system.SetMinBounceSpeed(.1)

# In[Define frictional properties]
mu_f=.5
mu_b=.01
mu_r=.4
mu_s=.4

# In[create material properties]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(mu_f)
material.SetDampingF(mu_b)
material.SetCompliance (0.00001)
material.SetComplianceT(0.00001)
material.SetRollingFriction(mu_r)
material.SetSpinningFriction(mu_s)
material.SetComplianceRolling(0.00001)
material.SetComplianceSpinning(0.0001)
# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(3, 1, 3) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(3, 1, 3)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall to enclose the robot]

# Geometry of walls
wwidth=.1
wheight=.1
wlength=1.5

meh=np.array([1,-1])

for i in(meh):
    # Create walls 
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(1.5*i, wheight/2, 0))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wwidth, wheight, wlength) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wwidth, wheight, wlength)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

for i in(meh):
    # Create walls
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(0, wheight/2, 1.5*i))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wlength, wheight, wwidth) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wlength,wheight, wwidth)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)
    
# diameter of cylinder and robots
diameter=.08 
# mass
mr=.18
# height of cylinder
height=0.12
# half height of cylinder
hhalf=.06
# calculate volume
volume=np.pi*.25*height*(diameter)**2
# calculate density of robot
rowr=mr/volume
# Force
F=.5
constfun = chrono.ChFunction_Const(F)

# Create bots    
bot = chrono.ChBody()
bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
bot.SetPos(chrono.ChVectorD(0,.06,0))
bot.SetMaterialSurface(material)

# give ID number
bot.SetId(1)
    
# collision model
bot.GetCollisionModel().ClearModel()
bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,.06) # hemi sizes
bot.GetCollisionModel().BuildModel()
bot.SetCollide(True)
bot.SetBodyFixed(False)
my_system.Add(bot)
myforcex = chrono.ChForce()
bot.AddForce(myforcex)
myforcex.SetMode(chrono.ChForce.FORCE)
myforcex.SetF_x(constfun)
myforcex.SetVrelpoint(chrono.ChVectorD(0,-.03,.02))
myforcex.SetDir(chrono.ChVectorD(1,0,0))

myforcex2 = chrono.ChForce()
bot.AddForce(myforcex2)
myforcex2.SetMode(chrono.ChForce.FORCE)
myforcex2.SetF_x(constfun)
myforcex2.SetVrelpoint(chrono.ChVectorD(0,-.03,-.02))
myforcex2.SetDir(chrono.ChVectorD(1,0,0))




col_y = chrono.ChColorAsset()
col_y.SetColor(chrono.ChColor(0.44, .11, 52))
bot.AddAsset(col_y)




# In[IRRLICHT code]

myapplication = chronoirr.ChIrrApp(my_system, 'PyChrono example', chronoirr.dimension2du(1024,768))

myapplication.AddTypicalSky()
myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
myapplication.AddLightWithShadow(chronoirr.vector3df(2,4,2),chronoirr.vector3df(0,0,0),9,1,9,50)                

            # ==IMPORTANT!== Use this function for adding a ChIrrNodeAsset to all items
                        # in the system. These ChIrrNodeAsset assets are 'proxies' to the Irrlicht meshes.
                        # If you need a finer control on which item really needs a visualization proxy in
                        # Irrlicht, just use application.AssetBind(myitem); on a per-item basis.

myapplication.AssetBindAll();

                        # ==IMPORTANT!== Use this function for 'converting' into Irrlicht meshes the assets
                        # that you added to the bodies into 3D shapes, they can be visualized by Irrlicht!
myapplication.AssetUpdateAll();

            # If you want to show shadows because you used "AddLightWithShadow()'
            # you must remember this:
myapplication.AddShadowAll();

# In[ Run the simulation]

count=0

myapplication.SetTimestep(.01)
myapplication.SetTryRealtime(True)

# In[Converts Quaternions to eulers]
def quaternion_to_euler(x, y, z, w):


        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        X = math.degrees(math.atan2(t0, t1))

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        Y = math.degrees(math.asin(t2))

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        Z = math.degrees(math.atan2(t3, t4))

        return X, Y, Z

while(myapplication.GetDevice().run()):
    rott0.append(obj[i].GetRot().e0)
    rott1.append(obj[i].GetRot().e1)
    rott2.append(obj[i].GetRot().e2)
    rott3.append(obj[i].GetRot().e3)
            
    
    myapplication.BeginScene()
    myapplication.DrawAll()
    myapplication.DoStep()
    myapplication.EndScene()
