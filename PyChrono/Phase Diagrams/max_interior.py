# -*- coding: utf-8 -*-
"""
Created on Tue Dec 24 10:54:58 2019

@author: dmulr
"""
import numpy as np
import math as math


nb=200             # number of robots
diameter=.04
R1=(diameter*nb/(np.pi*2))+.1 
Rin=R1-.1
ngrans=int(R1/(diameter*(1+np.sqrt(2))))


ri=np.zeros((1,2*ngrans))
ni=np.zeros((1,2*ngrans))


for i in range(2*ngrans):
    if i%2==0:
        dn=diameter
        #print(dn)
    else:
        dn=diameter*np.sqrt(2)
        #print(dn)
    ri[:,i]=Rin-i*((diameter/2)*(1+np.sqrt(2)))
    ni[:,i]=np.floor((ri[:,i]*np.pi*2)/dn)

print(ni)
print(ngrans)

