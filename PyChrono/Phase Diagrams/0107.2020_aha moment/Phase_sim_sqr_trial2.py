'''

author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 11/19/19
Phase diagram generator
'''

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np

import timeit
from Sim_objects import Material,Floor,Interior,Ball,Wall,Box,ExtractData,MyReportContactCallback,Controller
from myconfig import *

start = timeit.default_timer()

# In[Set Path]
#chrono.SetChronoDataPath("C:/Chrono/Builds/chrono-develop/bin/data/")
#chrono.SetChronoDataPath("C:/Users/dmulr/Documents/data/")
chrono.SetChronoDataPath("C:/Users/dmulr/OneDrive/Documents/data/")
#chrono.SetChronoDataPath("D:/WaveLab/Soft Robotics/Chrono/data/")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.SetSolverType(chrono.ChSolver.Type_SOR_MULTITHREAD)
#my_system.SetTol(1e-6)
#my_system.SetSolverType(chrono.ChSolver.Type_APGD)
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
my_system.SetMaxItersSolverSpeed(300)



material=Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs)

material2=Material(mu_f2,mu_b2,mu_r2,mu_s2,C,Ct,Cr,Cs)
# In[Create Floor]

body_floor=Floor(material2,length,tall)
my_system.Add(body_floor)



# In[Create txt file]
variables=([nb,ni,nr,k,mag,magd,ratio,Rb,R1,tp,tj])

texts=["number of boundary(n/a)=",
       "\r\n number of interior(n/a)=",
       "\r\n nb over ni(n/a)=",
       "\r\n number of interior(n/a)="
       "\r\n Spring constant(N/m)=",
       "\r\n Magnitude of force on bots(N)=",
       "\r\n magnitude of force on ball (N)=",
       "\r\n Ratio of ball to robot=",
       "\r\n Diameter of ball (m)=",
       "\r\n diameter of Robot (m)=",
       "\r\n Time to pull=",
       "\r\n Time to jam=",
       ]

f= open(sim+"variables.txt","w+")

for i in range(np.size(variables)):
    
    f.write(texts[i]+str(variables[i]) )



# In[Create Robots]
for i in range(nb): 
    theta=i*2*np.pi/nb
    x=R1*np.cos(theta)
    y=.5*height
    z=R1*np.sin(theta)
    # Create bots    
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetMaterialSurface(material)
    # rotate them
    rotation1 = chrono.ChQuaternionD()
    rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
    bot.SetRot(rotation1)
    # give ID number
    bot.SetId(i)
    # collision model
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,hhalf) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    
    #    theta=i*2*np.pi/nb
#    x=R1*np.cos(theta)
#    y=.5*height
#    z=R1*np.sin(theta)
#    

#    # Create bots    
#    bot = chrono.ChBody()
#    bot = chrono.ChBodyEasyBox(diameter,height,diameter,rowr)
#    bot.SetPos(chrono.ChVectorD(x,y,z))
#    bot.SetMaterialSurface(material)
#    # rotate them
#    rotation1 = chrono.ChQuaternionD()
#    rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
#    bot.SetRot(rotation1)
#    # give ID number
#    bot.SetId(i)
#    # collision model
#    bot.GetCollisionModel().ClearModel()
#    bot.GetCollisionModel().AddBox(diameter/2,hhalf,diameter/2) # hemi sizes
#    bot.GetCollisionModel().BuildModel()
#    bot.SetCollide(True)
#    bot.SetBodyFixed(False)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,bot,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    my_system.AddLink(pt)
    # Apply forces to active bots
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetDir(chrono.VECT_X)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,y,z))
        #myforcex.SetMforce(mag)
        force.append(myforcex)
        col_y = chrono.ChColorAsset()
        col_y.SetColor(chrono.ChColor(0.44, .11, 52))
        bot.AddAsset(col_y)
    # passive robots
    else:
        col_g = chrono.ChColorAsset()
        col_g.SetColor(chrono.ChColor(0, 1, 0))
        bot.AddAsset(col_g)   
    # Attach springs    
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground1=chrono.ChLinkSpring()
      # Identify points to be attatched to the springs 
        ground.SetName("ground")
        p1=0
        p2=diameter/2
        p3=0
        p4=-diameter/2
        h=0
        
        # Attatches first springs
        ground.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
        ground.Set_SpringF(k)

        ground.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        ground.Set_SpringR(spring_b)
        my_system.AddLink(ground)
        Springs.append(ground)

    # Last spring
    if i==nb-1:        
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(bot, obj[0], True, chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
        ground.Set_SpringF(k)
        ground.Set_SpringRestLength(rl)
        ground.Set_SpringRestLength(rl)
        ground.Set_SpringR(spring_b)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground) 
    my_system.Add(bot)
    obj.append(bot)
# In[Create Interior]
for i in range(n.size):
    print(i)
    if i%2==0:
        diameter3=diameter2*(2**.5)
    else:
        diameter3=diameter2
    for j in range(n[i]):
        print(j)
        R2=diameter3*n[i]/(np.pi*2)
        x=R2*np.cos(j*2*np.pi/n[i])
        y=.5*height
        z=R2*np.sin(j*2*np.pi/n[i])

        Interior(x,y,z,i,diameter3,height,rowp,R2,material,obj,my_system,body_floor)
# In[Create Ball]
Balls=[]
Ball(rx,ry,rz,Rb,height,hhalf,rowb,material2,obj,my_system,forceb,body_floor,Balls)

#RL=Rb
#ball=[]
#Box(rx,ry,rz,Rb,RL,height,hhalf,rowr,material,obj,my_system,forceb,body_floor,Balls)
# In[Create Wall]  
# x position of wall
z =  0      # Z position of wall
rotate = np.pi/2    # rotate wall
length = 10     # length of wall
height = .25    # height of wall
width = .1      # width of wall
y = height/1.9  # initial y position of wall
x=-R1-width
Wall(x,y,z,rotate,length,height,width,material,my_system)


z =  .55      # Z position of wall
rotate = np.pi/6    # rotate wall
length = 2     # length of wall
height = .25    # height of wall
width = .1      # width of wall
y = height/1.9  # initial y position of wall
x=-R1-width-.3
#Wall(x,y,-z,rotate,length,height,width,material,my_system)

#Wall(x,y,z,-rotate,length,height,width,material,my_system)
# In[ Create empty matrices to be filled]
Xpos=[]
Ypos=[]
Zpos=[]

Xforce=[]
Yforce=[]
Zforce=[]

# Contact forces 
Xcontact=[]
Ycontact=[]
Zcontact=[]

# empty temporary velocity matrices
Xvel=[]
Yvel=[]
Zvel=[]

templ=[]
ttemp=[]
# Rotation Positions
rott0=[]
rott1=[]
rott2=[]
rott3=[]

xunit=[]
yunit=[]
zuni=[]
coord=[]
Fm=[]
cpoint=[]
ballp=[]
# VERY important
count=0
# Contact points
cx=[]
cy=[]
cz=[]
nc=[]

# Contact forces

Fxct=[]
Fyct=[]
Fzct=[]
xx=[]
zz=[]
my_rep = MyReportContactCallback()
# In[Pov RAY]
if record==1:
    
    script_dir = os.path.dirname("povvideofiles"+sim+"/")
    pov_exporter = postprocess.ChPovRay(my_system)

    # Sets some file names for in-out processes.
    pov_exporter.SetTemplateFile(chrono.GetChronoDataPath() + "_template_POV.pov")
    pov_exporter.SetOutputScriptFile("rendering"+str(sim)+".pov")
    pov_exporter.SetOutputDataFilebase("my_state")
    pov_exporter.SetPictureFilebase("picture")

    # create folders
    if not os.path.exists("output"+str(sim)):
        os.mkdir("output"+str(sim))
    if not os.path.exists("anim"+str(sim)):
            os.mkdir("anim"+str(sim))
    pov_exporter.SetOutputDataFilebase("output"+str(sim)+"/my_state")
    pov_exporter.SetPictureFilebase("anim"+str(sim)+"/picture")
    pov_exporter.SetCamera(chrono.ChVectorD(0,3,0), chrono.ChVectorD(0,0,0), 90)# specifiy camera location
    pov_exporter.AddAll()
    pov_exporter.ExportScript()

    #In[Run the simulation]

    count=0
    t=tstep*count 
    while (my_system.GetChTime() < tend) :
        # time 
        print ('time=', my_system.GetChTime() )
        t=tstep*count
        my_rep.ResetList()
        Controller(my_system,force,mag,botcall,i,tset,Fm,nb,k,templ,t,rl,rlmax,Springs,forceb,jamcall,rlj,rljmax,tj,kj,mag2,magf,tp,obj,mag3)
        ExtractData(obj,nb,nt,Xpos,Ypos,Zpos,Xforce,Yforce,Zforce,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,ballp,Balls,Springs,Fm,templ)
        
        my_system.GetContactContainer().ReportAllContacts(my_rep)

        crt_list = my_rep.GetList()
        nc.append(my_system.GetContactContainer().GetNcontacts())
        cx.append(crt_list[0])
        cy.append(crt_list[1])
        cz.append(crt_list[2])
        Fxct.append(crt_list[3])
        Fyct.append(crt_list[4])
        Fzct.append(crt_list[5])
        # Extract data
        #ExtractData2(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj)  
        # Track center of robot for RL 
        Xpostemp=[]
        Zpostemp=[]
        for i in range(nb):
            Xpostemp.append(obj[i].GetPos().x)
            Zpostemp.append(obj[i].GetPos().z)
     
        Xpostemp=np.asarray(Xpostemp)
        Zpostemp=np.asarray(Zpostemp)
        
        Xavg=np.mean(Xpostemp)
        Zavg=np.mean(Zpostemp)
        
        ttemp.append(t)
        count=count+1
        my_system.DoStepDynamics(tstep)
        print ('time=', my_system.GetChTime())
        pov_exporter.SetCamera(chrono.ChVectorD(Xavg,3,Zavg), chrono.ChVectorD(Xavg,0,Zavg), 90)# specifiy camera location
        pov_exporter.AddAll()
        pov_exporter.ExportScript()
        # Export every 15th frame 
        if count%15==0:
            pov_exporter.ExportData()  
# In[Irrlecht]
else:
    myapplication = chronoirr.ChIrrApp(my_system,sim, chronoirr.dimension2du(1600,1200))
    myapplication.AddTypicalSky()
    myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
    myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
    myapplication.AddLightWithShadow(chronoirr.vector3df(2,5,2),chronoirr.vector3df(2,2,2),10,2,10,120)
    myapplication.DrawAll               
    myapplication.AssetBindAll();
    myapplication.AssetUpdateAll();
    myapplication.AddShadowAll();
    
    count=0
# Time step
    myapplication.SetTimestep(tstep)
    myapplication.SetTryRealtime(False)
    while(myapplication.GetDevice().run()):
        my_rep.ResetList()
        myapplication.BeginScene()
        myapplication.DrawAll()
        print ('time=', my_system.GetChTime())
        t=tstep*count  
        Controller(my_system,force,mag,botcall,tset,Fm,nb,k,templ,t,rl,rlmax,Springs,forceb,jamcall,rlj,rljmax,tj,kj,mag2,magf,tp,obj,mag3,Xpos,Ypos,Zpos,tstep,height,xx,zz)
        #ExtractData2(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj)  
        ExtractData(obj,nb,nt,Xpos,Ypos,Zpos,Xforce,Yforce,Zforce,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,ballp,Balls,Springs,Fm,templ)
        
        my_system.GetContactContainer().ReportAllContacts(my_rep)

        crt_list = my_rep.GetList()
        nc.append(my_system.GetContactContainer().GetNcontacts())
        cx.append(crt_list[0])
        cy.append(crt_list[1])
        cz.append(crt_list[2])
        Fxct.append(crt_list[3])
        Fyct.append(crt_list[4])
        Fzct.append(crt_list[5])
        ttemp.append(t)
        count=count+1
        myapplication.DoStep()
        myapplication.EndScene()
# Close the simulation if time ends

        if t > tend:
            myapplication.GetDevice().closeDevice()
nc=np.asarray(nc)


lengthm=np.amax(nc)


tryme=cx[1]



# In[Convert plist to matrices]
Xpos=np.asarray(Xpos)
Ypos=np.asarray(Ypos)
Zpos=np.asarray(Zpos)

rott0=np.asarray(rott0)
rott1=np.asarray(rott1)
rott2=np.asarray(rott2)
rott3=np.asarray(rott3)

templ=np.asarray(templ)

Xforce=np.asarray(Xforce)
Yforce=np.asarray(Yforce)
Zforce=np.asarray(Zforce)

Xcontact=np.asarray(Xcontact)
Ycontact=np.asarray(Ycontact)
Zcontact=np.asarray(Zcontact)

Xvel=np.asarray(Xvel)
Yvel=np.asarray(Yvel)
Zvel=np.asarray(Zvel)
Fm=np.asarray(Fm)

ballp=np.asarray(ballp)

xx=np.asarray(xx)
zz=np.asarray(zz)
# In[Create empty arrays]
# position
qx=np.zeros((nt,count))
qy=np.zeros((nt,count))
qz=np.zeros((nt,count))

# empty toational matrices
rot0=np.zeros((nt,count))
rot1=np.zeros((nt,count))
rot2=np.zeros((nt,count))
rot3=np.zeros((nt,count))


# contact forces
Fxc=np.zeros((nt,count))
Fyc=np.zeros((nt,count))
Fzc=np.zeros((nt,count))
# total forces
Fxt=np.zeros((nt,count))
Fyt=np.zeros((nt,count))
Fzt=np.zeros((nt,count))
# Spring length
SL=np.zeros((nb,count))

# Velocity empty matrices
Xv=np.zeros((nt,count))
Yv=np.zeros((nt,count))
Zv=np.zeros((nt,count))

# Membrane force
Fmem=np.zeros((nb,count))

#Create empty contact matrices
xc=np.zeros((lengthm,count))
yc=np.zeros((lengthm,count))
zc=np.zeros((lengthm,count))
# Contact forces
Fcx=np.zeros((lengthm,count))
Fcy=np.zeros((lengthm,count))
Fcz=np.zeros((lengthm,count))
# In[Fill the matrices]
for i in range(count):    
    # fill the position matrices
    qx[:,i]=Xpos[nt*i:nt*i+nt]
    qy[:,i]=Ypos[nt*i:nt*i+nt]  
    qz[:,i]=Zpos[nt*i:nt*i+nt]  
  
    # fill the rotational matrices  
    rot0[:,i]=rott0[nt*i:nt*i+nt]
    rot1[:,i]=rott1[nt*i:nt*i+nt]
    rot2[:,i]=rott2[nt*i:nt*i+nt]
    rot3[:,i]=rott3[nt*i:nt*i+nt]
    
    # fill the total force matrices
    Fxt[:,i]=Xforce[nt*i:nt*i+nt] 
    Fyt[:,i]=Yforce[nt*i:nt*i+nt] 
    Fzt[:,i]=Zforce[nt*i:nt*i+nt] 
    
    # fill the contact force matrices
  #  Fxc[:,i]=Xcontact[nt*i:nt*i+nt] 
  #  Fyc[:,i]=Ycontact[nt*i:nt*i+nt] 
   # Fzc[:,i]=Zcontact[nt*i:nt*i+nt]
    
    Xv[:,i]=Xvel[nt*i:nt*i+nt]
    Yv[:,i]=Yvel[nt*i:nt*i+nt]
    Zv[:,i]=Zvel[nt*i:nt*i+nt]
    
    SL[:,i]=templ[nb*i:nb*i+nb]
    Fmem[:,i]=Fm[nb*i:nb*i+nb]       

print(nc[0])
for i in range(count):

    ind=nc[i]
    tryme=cx[i]
    tryme2=cy[i]
    tryme3=cz[i]
    tryme4=Fxct[i]
    tryme5=Fyct[i]
    tryme6=Fzct[i]
    # convert to array
    tryme=np.asarray(tryme)
    tryme2=np.asarray(tryme2)
    tryme3=np.asarray(tryme3)
    tryme4=np.asarray(tryme4)
    tryme5=np.asarray(tryme5)
    tryme6=np.asarray(tryme6)
    
    # fill array position
    xc[0:ind,i]=np.transpose(tryme)
    yc[0:ind,i]=np.transpose(tryme2)
    zc[0:ind,i]=np.transpose(tryme3)

# Fill array forces
    Fcx[0:ind,i]=np.transpose(tryme4)
    Fcy[0:ind,i]=np.transpose(tryme5)
    Fcz[0:ind,i]=np.transpose(tryme6)            
# In[Save and export out to npz file]    
np.savez(sim+".npz",allow_pickle=True,
         Fxc=Fxc,
         Fyc=Fyc,
         Fzc=Fzc,
         Fxt=Fxt,
         Fyt=Fyt,
         Fzt=Fzt,
         qx=qx,
         qy=qy,
         qz=qz,
         nb=nb,
         ni=ni,
         mr=mr,
         mp=mp,
         k=k,
         rowr=rowr,
         rowp=rowp,
         height=height,
         diameter=diameter,
         volume=volume,
         ttemp=ttemp,
         count=count,
         rot0=rot0,
         rot1=rot1,
         rot2=rot2,
         rot3=rot3,
         botcall=botcall,
         SL=SL,
         Xv=Xv,
         Yv=Yv,
         Zv=Zv,
         sim=sim,
         Fmem=Fmem,
         ballp=ballp,
         xc=xc,
         yc=yc,
         zc=zc,
         nc=nc,
         Fcx=Fcx,
         Fcy=Fcy,
         Fcz=Fcz)
stop = timeit.default_timer()

# In[Print time out]
runtime=stop-start
runtime=runtime*(1/60)
print("Total runtime: "+str(runtime)+" minutes")