# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 14:34:30 2019

@author: dmulr
"""

# In[section1]
"""Import libraries"""
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np




#In[Material PRoerties]
chrono.SetChronoDataPath("data/")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
#my_system.SetSolverType(chrono.ChSolver.Type_BARZILAIBORWEIN)
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.001)
my_system.SetMaxItersSolverSpeed(1000)
# In[surface materialcreate material]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(1)
material.SetDampingF(.0)
material.SetCompliance (0.00001)
material.SetComplianceT(0.00001)
material.SetRollingFriction(.4)
material.SetSpinningFriction(.4)
material.SetComplianceRolling(0.00001)
material.SetComplianceSpinning(0.00001)



# In[Create element]
obj=[]
radius=.1
rho=1000
V=(16/3)*np.pi*(radius)**3
mass=rho*V
k=10000
b=0

x=np.array([[1,0,0,1]])
y=np.array([[1,1,0,0]])

for i in range(4):
    print(i)
    if i==0:
        x=2*radius
        y=2*radius
        
    if i==1:
        x=0
        y=2*radius
    if i==2:
        x=0
        y=0
    if i==3:
        x=2*radius
        y=0
                
    element = chrono.ChBody()
    element = chrono.ChBodyEasySphere(radius,rho)
    element.SetPos(chrono.ChVectorD(x,y,0))
    element.SetMaterialSurface(material)
    element.SetId(i)
    element.GetCollisionModel().ClearModel()
    element.GetCollisionModel().AddSphere(radius) # hemi sizes
    element.GetCollisionModel().BuildModel()
    element.SetCollide(True)
    col_g = chrono.ChColorAsset()
    col_g.SetColor(chrono.ChColor(0, 1, 0))     #Green
    element.AddAsset(col_g)
    
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(obj[i-1], element, False, chrono.ChVectorD(obj[i-1].GetPos().x,obj[i-1].GetPos().y ,0) ,chrono.ChVectorD(x,y,0))
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(2*radius)
        my_system.AddLink(ground)
    
    if i==3:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(element,obj[0], False,chrono.ChVectorD(x,y,0),chrono.ChVectorD(obj[0].GetPos().x,obj[0].GetPos().y ,0))
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(2*radius)
        my_system.AddLink(ground)
    
    ground=chrono.ChLinkSpring()
       ground.SetName("ground")
       ground.Initialize(obj[i-1], element, False, chrono.ChVectorD(obj[i-1].GetPos().x,obj[i-1].GetPos().y ,0) ,chrono.ChVectorD(x,y,0))
       ground.Set_SpringK(k)
       ground.Set_SpringR(b)
       ground.Set_SpringRestLength(2*radius)
        my_system.AddLink(ground)
        
    if i==2 or i==3:
        
        element.SetBodyFixed(True)
    else:
        element.SetBodyFixed(False)
    
    
    my_system.Add(element)
    obj.append(element)
    
    
    





application = chronoirr.ChIrrApp (my_system,"Slider-Crank Demo 1",chronoirr.dimension2du(800, 600),False,True)                                
application.AddTypicalLogo();
application.AddTypicalSky();
application.AddTypicalLights();
application.AddTypicalCamera(chronoirr.vector3df(2, 0, 2),chronoirr.vector3df(0, 0, 0));          

  ## Let the Irrlicht application convert the visualization assets.
application.AssetBindAll()
application.AssetUpdateAll()


  ## 6. Perform the simulation.

  ## Specify the step-size.
application.SetTimestep(0.01)
application.SetTryRealtime(True)

while (application.GetDevice().run()):

    ## Initialize the graphical scene.
    application.BeginScene()
    
    ## Render all visualization objects.
    application.DrawAll()

    ## Draw an XZ grid at the global origin to add in visualization.
    
    chronoirr.ChIrrTools.drawGrid(
                                  application.GetVideoDriver(), 1, 1, 20, 20,
                                  chrono.ChCoordsysD(chrono.ChVectorD(0, 0, 0), chrono.Q_from_AngX(chrono.CH_C_PI_2)),
                                  chronoirr.SColor(255, 80, 100, 100), True)

    ## Advance simulation by one step.
    application.DoStep()

    ## Finalize the graphical scene.
    application.EndScene()

