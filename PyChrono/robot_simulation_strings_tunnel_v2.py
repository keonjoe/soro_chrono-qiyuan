
# In[Header]

"""
author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 10/9/19
"""

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit


start = timeit.default_timer()

# In[Set Path]
chrono.SetChronoDataPath("data/")


# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.00001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.00001)
my_system.SetMaxItersSolverSpeed(1000)
my_system.SetMinBounceSpeed(.2)

# In[Define frictional properties]
mu_f=.1
mu_b=.01
mu_r=.1
mu_s=.1

# In[create material properties]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(mu_f)
material.SetDampingF(mu_b)
material.SetCompliance (0.00001)
material.SetComplianceT(0.00001)
material.SetRollingFriction(mu_r)
material.SetSpinningFriction(mu_s)
material.SetComplianceRolling(0.00001)
material.SetComplianceSpinning(0.0001)
# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(3, 1, 3) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(3, 1, 3)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall to enclose the robot]

# Geometry of walls
wwidth=.1
wheight=.1
wlength=1.5

# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(10, 1, 10) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(10, 1, 10)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall]

# In[Create wall]

wwidth=.1
wheight=.1
wlength=1.5

meh=np.array([1,-1])
#for i in(meh):
#    wall = chrono.ChBody()
#    wall.SetBodyFixed(True)
#    wall.SetPos(chrono.ChVectorD(1.5*i, wheight/2, 0))
#    wall.SetMaterialSurface(material)
#    # Collision shape
#    wall.GetCollisionModel().ClearModel()
#    wall.GetCollisionModel().AddBox(wwidth, wheight, wlength) # hemi sizes
#    wall.GetCollisionModel().BuildModel()
#    wall.SetCollide(True)
#    # Visualization shape
#    wall_shape = chrono.ChBoxShape()
#    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wwidth, wheight, wlength)
#    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
#    wall.GetAssets().push_back(wall_shape)
#    wall_texture = chrono.ChTexture()
#    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
#    wall.GetAssets().push_back(wall_texture)
#    my_system.Add(wall)

for i in(meh):
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(0, wheight/2, 1.5*i))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wlength, wheight, wwidth) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wlength,wheight, wwidth)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)


# In[Additional walls]
wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.7, wheight/2, -.85))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)

# In[Tunnel wall]
wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.2, wheight/2, -.75))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)



wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.7, wheight/2, .75))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)

# In[Tunnel wall]
wall = chrono.ChBody()
wall.SetBodyFixed(True)
wall.SetPos(chrono.ChVectorD(1.2, wheight/2, .85))
wall.SetMaterialSurface(material)
# Collision shape
wall.GetCollisionModel().ClearModel()
wall.GetCollisionModel().AddBox(.1, wheight, .3) # hemi sizes
wall.GetCollisionModel().BuildModel()
wall.SetCollide(True)
# Visualization shape
wall_shape = chrono.ChBoxShape()
wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(.1,wheight, .3)
wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
wall.GetAssets().push_back(wall_shape)
wall_texture = chrono.ChTexture()
wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
wall.GetAssets().push_back(wall_texture)
my_system.Add(wall)


# In[cylinder dimmensions/geometries]

# number of robots
nb=87

# number of interior robots
n=np.array([93,86,77,69,61,52,44,33,22,12,4])

# sum them for total number of interior
ni=np.sum(n)

# total number of bots and particles
nt=nb+ni

# diameter of cylinder and robots
diameter=.07 

# Radius of postions
R1=(diameter*nb/(np.pi*2))+.1

# mass
mr=.18

# mass of particles
mp=.01

# height of cylinder
height=0.12

# half height of cylinder
hhalf=.06

# calculate volume
volume=np.pi*.25*height*(diameter)**2

# calculate density of robot
rowr=mr/volume

# calculate density of particles
rowp=mp/volume

# spring constant
k=100

# damping
b=.05

# resting length
rl=0.00

# max resting length
rlmax=diameter

# empty matrix of bots and particles
obj=[]

# empty matrix of springs
Springs=[]


# In[External forces]
constfun = chrono.ChFunction_Const(10)
constfun0 = chrono.ChFunction_Const(0) 


  
# Empty matrix      
botcall=np.zeros((1,nb))

# ID number of robots to be active
active=np.array([])

# For robots that are active fill botcall==1
for i in (active):
    botcall[:,i]=1

# In[Create Bots]
for i in range (nb):
    
    # Initial postion of each bot
    theta=i*2*np.pi/nb
    x=R1*np.cos(theta)
    y=.5*height
    z=R1*np.sin(theta)
    
    # Create bots    
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetMaterialSurface(material)
    
    # rotate them
    rotation1 = chrono.ChQuaternionD()
    rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
    bot.SetRot(rotation1)
    
    # give ID number
    bot.SetId(i)
    
    # collision model
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,hhalf) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    bot.SetBodyFixed(False)
    
    # Apply forces to active bots
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetF_x(constfun)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
        myforcex.SetDir(chrono.ChVectorD(1,0,0))
        col_y = chrono.ChColorAsset()
        col_y.SetColor(chrono.ChColor(0.44, .11, 52))
        bot.AddAsset(col_y)
    
    # passive robots
    else:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetF_x(constfun0)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
        myforcex.SetDir(chrono.ChVectorD(1,0,0))
        col_g = chrono.ChColorAsset()
        col_g.SetColor(chrono.ChColor(0, 1, 0))
        bot.AddAsset(col_g)
        
    # Attatch springs    
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        p1=0
        p2=diameter/2
        p3=0
        p4=-diameter/2

        ground.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,0,p2), chrono.ChVectorD(p3,0,p4),False)
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground)
    # Last spring
        if i==nb-1:        
            ground=chrono.ChLinkSpring()
            ground.SetName("ground")
            ground.Initialize(bot, obj[0], True, chrono.ChVectorD(p1,0,p2), chrono.ChVectorD(p3,0,p4),False)
            ground.Set_SpringK(k)
            ground.Set_SpringR(b)
            ground.Set_SpringRestLength(rl)
            col1=chrono.ChColorAsset()
            col1.SetColor(chrono.ChColor(0,0,1))
            ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground) 
    my_system.Add(bot)
    obj.append(bot)        


# In[Create interiors]
for i in range(n.size):
    print(i)
    for j in range(n[i]):
        R2=diameter*n[i]/(np.pi*2)
        x=R2*np.cos(j*2*np.pi/n[i])
        y=.6*height
        z=R2*np.sin(j*2*np.pi/n[i])
        gran = chrono.ChBody()
        gran = chrono.ChBodyEasyCylinder(diameter/2, height/2,rowp)
        gran.SetPos(chrono.ChVectorD(x,y,z))
        gran.SetMaterialSurface(material)
        gran.SetId(i)
        gran.GetCollisionModel().ClearModel()
        gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/4) # hemi sizes
        gran.GetCollisionModel().BuildModel()
        gran.SetCollide(True)
        col_r = chrono.ChColorAsset()
        col_r.SetColor(chrono.ChColor(1, 0, 0))
        gran.AddAsset(col_r)
        my_system.Add(gran)
        obj.append(gran) 


        
# In[ Create empty matrices to be filled]
        
# empty temporary position matrices
Xpos=[]
Ypos=[]
Zpos=[]

# emptry temporary rotation matrices
rott0=[]
rott1=[]
rott2=[]
rott3=[]

# empty temporary velocity matrices
Xvel=[]
Yvel=[]
Zvel=[]

# empty temporary force matrices
Xforce=[]
Yforce=[]
Zforce=[]


# empty temporary contact force matrices
Xcontact=[]
Ycontact=[]
Zcontact=[]


templ=[]
ttemp=[]


count=0


# In[Irrlecht Simulation]


# In[ Run the simulation]

count=0

# Time step
h=.008

#myapplication.SetTimestep(h)
#myapplication.SetTryRealtime(True)

# In[Set up pov exporter]
pov_exporter = postprocess.ChPovRay(my_system)

# Sets some file names for in-out processes.
pov_exporter.SetTemplateFile("data/_template_POV.pov")
pov_exporter.SetOutputScriptFile("rendering.pov")
pov_exporter.SetOutputDataFilebase("my_state")
pov_exporter.SetPictureFilebase("picture")

# create folders
if not os.path.exists("output"):
    os.mkdir("output")
if not os.path.exists("anim"):
    os.mkdir("anim")
pov_exporter.SetOutputDataFilebase("output/my_state")
pov_exporter.SetPictureFilebase("anim/picture")

# Export out objects
pov_exporter.AddAll() 
pov_exporter.SetCamera(chrono.ChVectorD(1.3,3,0), chrono.ChVectorD(1.3,0,0), 90)# specifiy camera location
pov_exporter.ExportScript()
 #In[Run the simulation]
count=0
while (my_system.GetChTime() < 1) :
    


    for i in range(nb):
        var1=Springs[i].Get_SpringLength()
        
        if var1<rl:
            Springs[i].Set_SpringK(0)
        
        if var1>rlmax:
            Springs[i].Set_SpringK(10*k)
        else:
            Springs[i].Set_SpringK(k)
        t=h*count
    
    for i in range(nt):
            
            # Tempory named variables
            templ.append(var1)
            temp=obj[i].GetContactForce()
            tempx=obj[i].Get_Xforce()
            tempxx=obj[i].GetPos_dt()
            
            # Fill in the temportary  Total force matrices
            Xforce.append(tempx.x)
            Yforce.append((tempx.y))
            Zforce.append(tempx.z)
            
            # Fill in the temporary contact force matrices
            Xcontact.append(temp.x)
            Ycontact.append(temp.y)
            Zcontact.append(temp.z)
            
            # Fill in the temporary position matrices
            Xpos.append(obj[i].GetPos().x)
            Ypos.append((obj[i].GetPos().y))
            Zpos.append(obj[i].GetPos().z)
            
            # fill in the temporary rotational matrices
            rott0.append(obj[i].GetRot().e0)
            rott1.append(obj[i].GetRot().e1)
            rott2.append(obj[i].GetRot().e2)
            rott3.append(obj[i].GetRot().e3)
            
            # fill in the temporary velocity matrices
            Xvel.append(tempxx.x)
            Yvel.append(tempxx.y)
            Zvel.append(tempxx.z)
        
        
        
    ttemp.append(t)
    count=count+1

    my_system.DoStepDynamics(h)
    print ('time=', my_system.GetChTime())
    pov_exporter.ExportData()
# In[Convert plist to matrices]
Xpos=np.asarray(Xpos)
Ypos=np.asarray(Ypos)
Zpos=np.asarray(Zpos)

rott0=np.asarray(rott0)
rott1=np.asarray(rott1)
rott2=np.asarray(rott2)
rott3=np.asarray(rott3)


Xvel=np.asarray(Xvel)
Yvel=np.asarray(Yvel)
Zvel=np.asarray(Zvel)

templ=np.asarray(templ)

Xforce=np.asarray(Xforce)
Yforce=np.asarray(Yforce)
Zforce=np.asarray(Zforce)

Xcontact=np.asarray(Xcontact)
Ycontact=np.asarray(Ycontact)
Zcontact=np.asarray(Zcontact)
#SpringF=np.asarray(SpringF)
#print(SpringF)
# In[Create empty arrays]
# position
qx=np.zeros((nt,count))
qy=np.zeros((nt,count))
qz=np.zeros((nt,count))

# empty toational matrices
rot0=np.zeros((nt,count))
rot1=np.zeros((nt,count))
rot2=np.zeros((nt,count))
rot3=np.zeros((nt,count))


# contact forces
Fxc=np.zeros((nt,count))
Fyc=np.zeros((nt,count))
Fzc=np.zeros((nt,count))
# total forces
Fxt=np.zeros((nt,count))
Fyt=np.zeros((nt,count))
Fzt=np.zeros((nt,count))
# Spring length
SL=np.zeros((nt,count))

Xv=np.zeros((nt,count))
Yv=np.zeros((nt,count))
Zv=np.zeros((nt,count))


for i in range(count):
    print(i)
    
    
    # fill the position matrices
    qx[:,i]=Xpos[nt*i:nt*i+nt]
    qy[:,i]=Ypos[nt*i:nt*i+nt]  
    qz[:,i]=Zpos[nt*i:nt*i+nt]  
  
    # fill the rotational matrices  
    rot0[:,i]=rott0[nt*i:nt*i+nt]
    rot1[:,i]=rott1[nt*i:nt*i+nt]
    rot2[:,i]=rott2[nt*i:nt*i+nt]
    rot3[:,i]=rott3[nt*i:nt*i+nt]
    
    # fill in the velocity matrices
    Xv[:,i]=Xvel[nt*i:nt*i+nt]
    Yv[:,i]=Yvel[nt*i:nt*i+nt]
    Zv[:,i]=Zvel[nt*i:nt*i+nt]
    
    
    # fill the total force matrices
    Fxt[:,i]=Xforce[nt*i:nt*i+nt] 
    Fyt[:,i]=Yforce[nt*i:nt*i+nt] 
    Fzt[:,i]=Zforce[nt*i:nt*i+nt] 
    
    # fill the contact force matrices
    Fxc[:,i]=Xcontact[nt*i:nt*i+nt] 
    Fyc[:,i]=Ycontact[nt*i:nt*i+nt] 
    Fzc[:,i]=Zcontact[nt*i:nt*i+nt]
    
    # spring resting length
    SL[:,i]=templ[nt*i:nt*i+nt]
    
        
stop = timeit.default_timer()

runtime=stop-start
runtime=runtime/60


variables=([nb,ni,diameter,height,volume,mr,mp,k,b,mu_f,mu_b,mu_r,mu_s,rl,runtime])

texts=["number of boundary(n/a)=","\r\n number of interior(n/a)=","\r\n diameter(m)=","\r\n height of each robot(m)=","\r\n volume(m^3)=","\r\n mass of each robot(kg)=","\r\n mass of each particle(kg)=","\r\n spring constant(N/m)=","\r\n damping coefficent(Ns/m)=","\r\nsliding friction=","\r\nmaterial dampning=","\r\nRolling friction=","\r\nSpinning friction=","\r\nresting length=","\r\nruntime(minutes)="]

f= open("simulation_run"+str(1)+"variables.txt","w+")

for i in range(np.size(variables)):
    
    f.write(texts[i]+str(variables[i]) )
    
np.savez('resume1str.npz',allow_pickle=True,Fxc=Fxc,Fyc=Fyc,Fzc=Fzc,Fxt=Fxt,Fyt=Fyt,Fzt=Fzt,qx=qx,qy=qy,qz=qz,nb=nb,ni=ni,mr=mr,mp=mp,k=k,rowr=rowr,rowp=rowp,height=height,diameter=diameter,volume=volume,ttemp=ttemp,count=count,rot0=rot0,rot1=rot1,rot2=rot2,rot3=rot3,botcall=botcall,SL=SL,Xv=Xv,Yv=Yv,Zv=Zv)

