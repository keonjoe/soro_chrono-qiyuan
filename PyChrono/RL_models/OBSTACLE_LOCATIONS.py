# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 12:33:15 2020

@author: dmulr
"""

import numpy as np
from bridson import poisson_disc_samples
import math
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import animation





    

        


class obstacles:
    count=0    
    def __init__(self):
        self.index=obstacles.count
        obstacles.count += 1        

class line2d(obstacles):
    def __init__(self,x1,y1,x2,y2):
        obstacles.__init__(self)
        self.x1=x1
        self.x2=x2
        self.y1=y1
        self.y2=y2
        self.z1=0
        self.z2=0
        self.r1=np.array([[self.x1,self.y1,self.z1]])
        self.r2=np.array([[self.x2,self.y2,self.z2]])
        self.type='lineObstacle'
        
class circleObstacle(obstacles): 
    def __init__(self,x,y,R):
        obstacles.__init__(self)
        self.x=x
        self.y=y
        self.R=R
        self.z=0
        self.r=np.array([[self.x,self.y,self.z]])
        self.type='circleObstacle'
        
class bodies:
    count=0
    Cr = np.empty((0,3))
    Ctheta = np.empty((0,3))
    
    def __init__(self):
        self.index=bodies.count
        bodies.count += 1
        
class rigidBody2D(bodies):
    
    def __init__(self, x, y, m):
        bodies.__init__(self)
        self.x=x
        self.y=y
        self.z=0
        self.r=np.array([[self.x,self.y,self.z]])
        self.theta=np.array([[0,0,0]])
        self.m=m
        bodies.Cr = np.append(bodies.Cr, self.r, axis=0)
        bodies.Ctheta = np.append(bodies.Ctheta, self.theta, axis=0)
            
    
class circle(rigidBody2D):
    count=0
    
    def __init__(self, x, y, R, rho):
        circle.count += 1
        self.type='circle'
        self.R=R
        self.m=rho*np.pi*(self.R)**2
        rigidBody2D.__init__(self, x, y, self.m)
        
        
    def area(self):
        return np.pi*(self.R)**2
    
    def updateCoordinates(self,r0,theta0):
        self.r=r0
        self.theta=theta0
        bodies.Cr[self.index,:]=r0
        bodies.Ctheta[self.index,:]=theta0

        
def calculateRadiusMatrix(obj):
    bodies.RadialDist=np.zeros((bodies.count,bodies.count))
    for i in range(0,bodies.count):
        for j in range(0,bodies.count):
            bodies.RadialDist[i,j]=obj[i].R+obj[j].R   
    
def calculateDistances():
    bodies.distance=np.zeros((bodies.count,bodies.count))
    for i in range(0,bodies.count):
        t=bodies.Cr-bodies.Cr[i,:]
        t=np.linalg.norm(t,axis=1)
        bodies.distance[:,i]=t    

def detectRoughCollision():
    calculateDistances()
    t=bodies.distance-bodies.RadialDist
    rows, cols=np.where(t<0)
    t=np.transpose(np.array([rows,cols]))
    t=t[np.argsort(t[:, 0])]
    
    return t

def poisson_distribution(obj,rLim,xf2,yf2):
    t=np.array(poisson_disc_samples(width=xf2, height=yf2, r=rLim))
    for i in range(0,np.size(t[:,0])):
        #R=rLim*np.random.rand()/2
        R=rLim/3
        A=np.pi*R**2
        obj.append(circle(t[i,0],t[i,1],R,A))
        
    return obj

def rectangleObstacle(obj,x1,y1,x2,y2):
    obj.append(line2d(x1,y1,x2,y1))
    obj.append(line2d(x2,y1,x2,y2))
    obj.append(line2d(x2,y2,x1,y2))
    obj.append(line2d(x1,y2,x1,y1))
        
    return obj

def plotObj(obj):
    ax = plt.gca()
    c=3
    # change default range so that new circles will work
    ax.set_xlim((xf1-c, xf2+c))
    ax.set_ylim((yf1-c, yf2+c))

    for i in range (0,len(obj)):
        if(obj[i].type=='circle'):
            circle1 = plt.Circle((obj[i].x,obj[i].y),obj[i].R,color='r')
            ax.add_artist(circle1)

        if(obj[i].type=='lineObstacle'):
            l = mlines.Line2D([obj[i].x1,obj[i].x2], [obj[i].y1,obj[i].y2])
            ax.add_line(l)

        if(obj[i].type=='circleObstacle'):
            circle1 = plt.Circle((obj[i].x,obj[i].y),obj[i].R,color='b')
            ax.add_artist(circle1)

    plt.show()
    
    




obj=[]
xf1,yf1,xf2,yf2=(-1,-1,14,14)

xb=xf2-2
yb=yf2-2
rLim=2
obj=poisson_distribution(obj,rLim,xb,yb)
obj=rectangleObstacle(obj,xf1,yf1,xf2,yf2)



plotObj(obj)



np.savez("Obstacle.npz",allow_pickle=True,obj=obj)

